﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace MvcWeb.Controllers.Article
{
    public class ArticleController : Controller
    {

        // GET: Article
        public ActionResult Index()
        {
            ViewBag.Title = "列表页面";
            var requestJson = JsonConvert.SerializeObject("[{key:'3cc99f8a0d5c5ef62fd0508de88c4801',num:10}]");
            HttpContent httpContent = new StringContent(requestJson);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var httpClient = new HttpClient();
            var responseJson = httpClient.PostAsync("http://api.tianapi.com/social/", httpContent)
                .Result.Content.ReadAsStringAsync().Result;

            ViewBag.news = responseJson;
            return View();
        }

        // GET: Article/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Article/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Article/Create
        [System.Web.Http.HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Article/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Article/Edit/5
        [System.Web.Http.HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Article/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Article/Delete/5
        [System.Web.Http.HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET api/Account/List
        //[System.Web.Http.Route("getinfo")]
        public string getinfo([FromBody]userinfo jsoninfo)
        {
            return jsoninfo.name;
        }
    }
}

public class userinfo
{
    public string name { get; set; }
    public string sex { get; set; }
}
public class girlfirend
{
    public string mingzi { get; set; }
}
